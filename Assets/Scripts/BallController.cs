﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class BallController : MonoBehaviour
{

    Rigidbody2D rb;
    bool started;
    bool gameOver;
    public float rotation;
    public float upForce;
    //public AudioSource audioData;
    

    

    // Use this for initialization
    void Start()
    {
        
        rb = GetComponent<Rigidbody2D>();
        started = false;
        gameOver = false;
        //audioData = GetComponent<AudioSource>();

        
        
    }

    // Update is called once per frame
    void Update()
    {

        if (!started)
        {
            if (Input.GetMouseButtonDown(0))
            {
                started = true;
                rb.isKinematic = false;
                GameManager.instance.GameStart();

            }
        }

        else
        {
            if (!gameOver)
            {

                if (Input.GetMouseButtonDown(0))
                {

                    //audioData.Play();
                    transform.Rotate(0, 0, rotation);

                    rb.velocity = Vector2.zero;
                    Vector2 force = new Vector2(0, upForce);
                    rb.AddForce(force);
                }
            }
        }

    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        gameOver = true;
        GameManager.instance.GameEnd();

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "pipe" && !gameOver)
        {
            gameOver = true;
            GameManager.instance.GameEnd();
            rb.velocity = Vector2.zero;
            

        }
        if (collision.gameObject.tag == "ScoreCheck" && !gameOver)
        {
            ScoreManaging.instance.AddScore();
        }
        if (collision.gameObject.tag == "Ground")
        {
            
            rb.velocity = Vector2.zero;
        }
    }

}
