﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeController : MonoBehaviour {

    public float speed;
    public float verticalSpeed;
    Rigidbody2D rb2;
    


    // Use this for initialization
    void Start () {
       
        rb2 = GetComponent<Rigidbody2D>();
        MovePipe();
        InvokeRepeating("SwitchVertical", 1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void MovePipe()
    {
        rb2.velocity = new Vector2(-speed, verticalSpeed);

    }


    void SwitchVertical()
    {
        verticalSpeed = -verticalSpeed;
        rb2.velocity = new Vector2(-speed, verticalSpeed);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "RemovePipes")
        {
            Destroy(gameObject);
        }
    

    }

}
