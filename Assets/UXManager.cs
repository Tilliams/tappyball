﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UXManager : MonoBehaviour {

    public static UXManager instance;
    public GameObject startUI;
    public GameObject endGamePanel;
    public GameObject gameEndText;
    public Text scoreText;
    public Text highScoreText;
    public Text highScoreNumber;
    

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        scoreText.text = ScoreManaging.instance.score.ToString();
	}

    public void CommenceGame()
    {
        startUI.SetActive(false);
        
    }


    public void EndGame()
    {
        gameEndText.SetActive(true);
        endGamePanel.SetActive(true);
        highScoreText.text = "High Score: ";
        highScoreNumber.text = PlayerPrefs.GetInt("HighScore").ToString();
        
    }

    public void Retry()
    {
        SceneManager.LoadScene("LevelUn");
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
}
