﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    bool gameOver;
    public AudioSource audioData;

    void Awake()
    {
        
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        audioData = GetComponent<AudioSource>();

        gameOver = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GameStart()
    {
        UXManager.instance.CommenceGame();
        GameObject.Find("PipeSpawner").GetComponent<PipeSpawner>().StartSpawningPipes();

    }

    public void GameEnd()
    {
        audioData.Play();
        gameOver = false;
        GameObject.Find("PipeSpawner").GetComponent<PipeSpawner>().StopSpawningPipes();
        UXManager.instance.EndGame();
        ScoreManaging.instance.StopScore();
        UnityAddManager.instance.ShowAd();
        
    }
}
